#import kebutuhan mqtt
import paho.mqtt.client as mqtt

# On message yang berisikan histori-
# publisher dan subscirber
# berdasarkan waktu & ip
def on_message(client, userdata, message):
    nama_file = "history.txt"
    file = open(nama_file,'a')
    file.write(str(message.payload.decode('utf-8')))
    file.close()
    print("Data history berhasil direkap pada ", nama_file)

broker_addr = 'localhost' # Deklarasi broker address
port = 1886 # Deklarasi port

# Proses listening server
print("LISTENING ...")
server = mqtt.Client("SERVER") # Membuat instance baru dengan nama SERVER
server.connect(broker_addr,port) # melakukan koneksi ke broker dan port
server.on_message=on_message # Register fungsi / prosedur

server.subscribe("ip_publisher") # server melakukan subscribe ip address PUBLISHER
server.subscribe("ip_subscriber") # server melakukan subscribe ip address SUBSCRIBER

server.loop_forever() # LOOP forever agar server terus merekap interaksi pub dan sub
