# import kebutuhan MQTT
import paho.mqtt.client as mqtt
# import untuk penanggalan dan jam
from datetime import datetime
# import socket untuk kebutuhan get ip address
import socket

broker_addr = 'localhost' #deklarasi broker address
port = 1886 #deklarasi port

print("BIKIN INSTANSI BARU")
klayen = mqtt.Client("Publisher") #membuat client dengan nama PUBLISHER

print("koneksi ke broker")
klayen.connect(broker_addr, port) #melakukan koneksi ke broker

print("Anda sedang publish foto")
#Membuka foto pertama untuk dikirim ke broker
file_foto_1 = open("tzuyu.jpg","rb")
foto_satu = file_foto_1.read() #file foto yang di read sudah otomatis dalam bentuk biner
file_foto_1.close() #close agar tidak terjadi ambiguitas

#Membuka foto kedua untuk dikirim ke broker
file_foto_2 = open("jihyo.jpg",'rb')
foto_dua = file_foto_2.read()
file_foto_2.close()

#melakukan publish ke broker dengan topik tzuyu dan jihyo
klayen.publish("tzuyu",foto_satu,retain=True) # Retain True agar file foto tetap ada di broker
klayen.publish("jihyo",foto_dua, retain=True) # Retain True agar file foto tetap ada di broker
time_now = datetime.now() #Memasukan waktu sekarang kedalam variable
stringdate = time_now.strftime("%d-%b-%Y (%H:%M:%S)") # Format bentuk waktu agar mudah dibaca
print("Foto dipublish pada : ", stringdate)


# Dibawah ini fungsi untuk mengetahui IP address Publisher
# Untuk dikirim ke SERVER dan ditulis didalam file txt

hostname = socket.gethostname() # GET nama host laptop / pc kita
IPAddr ="\nPUBLISHER dengan ip address: " +  socket.gethostbyname(hostname) + " melakukan PUBLISH foto pada " + stringdate #Pesan yang akan dikirim ke server
print("Your Computer ip address is:" , IPAddr)
klayen.publish("ip_publisher", IPAddr) # Melakukan publish dengan topik ip_publisher

klayen.loop_start()
