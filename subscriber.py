#import paho mqtt
import paho.mqtt.client as mqtt 

#import time untuk mendapatkan time.sleep
import time 

#import datetime untuk mendapatkan format tanggal dan waktu 
import datetime

#import socket untuk mendapatkan ip network
import socket

#import os
import os

#fungsi untuk mengirimkan pesan yang ingin didapatkan dan pesan yang ingin dipublish 
def on_message(client, userdata, message):
	#menambahkan localtime untuk mendapatkan timestamp
	localtime=datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S)")

	#membuka file untuk menaruh hasil file yang ditransfer ke dalam file "Photo.jpg"
	string=message.payload
	P = open("Photo.jpg", "wb")
	P.write(string)
	P.close()

	#system akan clear screen saat akan memberikan notifikasi bahwa foto berhasil diunduh
	os.system("cls")

	#mendapatkan ip network 
	IP = "\nSUBSCRIBER dengan ip address: " + socket.gethostbyname(socket.gethostname()) +" melakukan SUBSCRIBE foto Pada " + localtime + '\n'
	hostname = socket.gethostname()

	#mempublish ip untuk di catat di server
	klien.publish("ip_subscriber", IP)

	#memberikan notifikasi jika sudah berhasil di unduh 
	print("---------------------------")
	print(localtime)
	print("  SUCCESS")
	print("---------------------------")

	#memberikan keterangan darimana foto ini berhasil diunduh 
	print(IP)	

#mendefinisikan alamat broker
alamat_broker="localhost"

#membuat instansi baru 
print("Membuat instansi baru..")
klien = mqtt.Client("P2")

#koneksikan dengan broker
print("Membuat koneksi ke broker..")
klien.connect(alamat_broker, port=1886)

#diberikan jeda agar komeksi terlihat 
time.sleep(1)

#system akan clear screen saat akan mengeluarkan pilihan 
os.system("cls")

#memberikan pilihan yang ingin di subscribe 
print("Pilh Foto yang diinginkan  :")
print("1. Tzuyu")
print("2. Jihyo")
apa = int(input("Masukkan Pilihan anda : "))#inputan yang dimasukan oleh user 

#memberikan kondisi dengan inputan yang dimasukkan 
if (apa==1) :
	klien.subscribe("tzuyu")
	klien.subscribe("ip_publisher")
	print("mengunduh foto tzuyu..")
	
elif(apa==2) :
	klien.subscribe("jihyo")
	klien.subscribe("ip_publisher")
	print("mengunduh foto jihyo..")
else :
	print("pilihan tidak tersedia")

#callback diaktifkan 
klien.on_message=on_message


#klien loop forever
while True :
	klien.loop(10)
	time.sleep(1)

#klien stop loop 
klien.loop_stop()


