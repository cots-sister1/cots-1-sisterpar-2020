# COTS 1 SisterPar 2020 PUBLISH FOTO (TOPIK 13)


## 1.  Deskripsi Program
> Program yang kami buat untuk memenuhi tugas COTS Mata Kuliah Sistem Terdistribusi 
    adalah membuat sebuah sistem dimana terdapat suatu instansi atau perorangan yang disebut **PUBLISHER** yang melakukan publish topik berupa foto 
    dan terdapat **SUBSCRIBER** yang dapat melakukan download file foto tersebut. dan juga terdapat **SERVER** yang dapat merekap ip dan waktu Publish foto dan Subscribe foto

## 2. Pembagian Tugas
Tugas ini dikerjakan oleh dua orang yaitu :
1.  Ferzi Samal Yerzi -Tugas : Pembuatan code untuk **publisher** dan **server**
2.  Ananda Fitri Karimah -Tugas : Pembuatan code untuk **subscriber**
    
## 3. Prosedur Installasi
Prosedur installasi yang dilakukan adalah :
1.  Installasi python versi 3 dan library python pip
2.  Menggunakan OS Linux Ubuntu 18 (optional)
3.  Installasi kebutuhan MQTT dengan pip install paho pada cmd atau terminal
4.  Melakukan installasi Mosquitto untuk menjalankan broker (linux dengan "sudo apt-get install mosquitto" ; pada windows dapat download mosquitto lalu install manual) 
